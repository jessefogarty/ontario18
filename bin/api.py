#!/usr/bin/env python3

from flask import Flask, request
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps


# create SQL *connection engine* using sqlalchemy
# DB needs to be .db for sqlalchemy
eng = create_engine('sqlite:///ontario18.db')

app = Flask(__name__)
api = Api(app)

# TODO: create home page or a redirect

class leaders_meta(Resource):
    # get definition is a url get request
    def get(self):
        # DB connection
        conn = eng.connect()
        # SQL Query w/ JSON response
        query = conn.execute('SELECT id, name, handle FROM leaders')
        return {'leaders': [i for i in query.cursor.fetchall()]}


class tweets(Resource):
    def get(self, leader_id):
        conn = eng.connect()
        # TODO: add placeholder for leader id query
        query = conn.execute('''
                SELECT leaders.id AS leader_id, leaders.name AS leader_name,
                tweets.post_date, tweets.html AS html_embed FROM leaders
                INNER JOIN tweets ON leaders.id = tweets.leader_id''')
        result = {
            'tweetDump': [dict(zip(tuple(query.keys()), i))
                          for i in query.cursor]}
        return result


api.add_resource(leaders_meta, '/leaders')
api.add_resource(tweets, '/leader/<string:leader_id>')

if __name__ == '__main__':
    app.run(host='192.168.0.20', port=80)
