#!/usr/bin/env python3
"""A simple twitter scraper module.

methods available:
- wynne_tweets
- ford_tweets
- horwath_tweets
- green_tweets
"""
import twitter
import sqlite3
import yaml
import time


class scraper():
    """Create instance of a scraper object."""

    def __init__(self):
        """Initialize counter and twitter API for the scraper."""
        # TODO: move SQL connector to __init__
        self.count = 0
        self.conf = yaml.load(open('creds2.yml'))
        self.ckey = self.conf['api']['ckey']
        self.cscr = self.conf['api']['cscr']
        self.atoken = self.conf['api']['atoken']
        self.atokscr = self.conf['api']['atokscr']

    def wynne_tweets(self):
        """Tell the scraper to fetch Kathleen_Wynne tweets."""
        self.api = twitter.Api(sleep_on_rate_limit=True,
                               consumer_key=self.ckey,
                               consumer_secret=self.cscr,
                               access_token_key=self.atoken,
                               access_token_secret=self.atokscr,)

        # BEGIN Tweet Scrape

        conn = sqlite3.connect('ontario18.db')
        cur = conn.cursor()

        wynne = 'Kathleen_Wynne'
        wynne_id = 1

        print('Collecting & Adding Tweets to the Databse...', '\n')
        time.sleep(2)
        print('...', '\n')

        tweets = self.api.GetUserTimeline(screen_name=wynne, include_rts=False,
                                          count=150)

        for tweet in tweets:
            self.count = self.count + 1
            print(self.count)
            print('Extracting Tweet Data & Obtaining Embed HTML...')
            tweetid = tweet.id
            """embed_html = html blockquote parsed from status_id"""
            embed_raw = self.api.GetStatusOembed(status_id=tweetid)
            embed_html = embed_raw['html']
            post_date = tweet.created_at
            # catch index errors where no URL supplied from tweet meta
            try:
                url = tweet.urls[0].url
            except IndexError:
                url = 'none'
            content = tweet.text
            favorites = tweet.favorite_count
            retweets = tweet.retweet_count
            cur.execute('''
                INSERT OR IGNORE INTO tweets
                (id, leader_id, post_date, url, content, favorite_count,
                retweet_count, html)
                VALUES (?,?,?,?,?,?,?,?)''', (tweetid, wynne_id, post_date,
                                              url, content, favorites,
                                              retweets, embed_html,))
        conn.commit()
        conn.close()

    def ford_tweets(self):
        """Tell the scraper to fetch tweets."""
        self.api = twitter.Api(sleep_on_rate_limit=True,
                               consumer_key=self.ckey,
                               consumer_secret=self.cscr,
                               access_token_key=self.atoken,
                               access_token_secret=self.atokscr,)

        # BEGIN Tweet Scrape

        conn = sqlite3.connect('ontario18.db')
        cur = conn.cursor()
        ford = 'fordnation'
        ford_id = 2

        print('Collecting & Adding Tweets to the Databse...', '\n')
        time.sleep(2)
        print('...', '\n')

        tweets = self.api.GetUserTimeline(screen_name=ford,
                                          include_rts=False, count=150)

        for tweet in tweets:
            self.count = self.count + 1
            print(self.count)
            print('Extracting Tweet Data & Obtaining Embed HTML...')
            tweetid = tweet.id
            """embed_html = html blockquote parsed from status_id"""
            embed_raw = self.api.GetStatusOembed(status_id=tweetid)
            embed_html = embed_raw['html']
            post_date = tweet.created_at
            # catch index errors where no URL supplied from tweet meta
            try:
                url = tweet.urls[0].url
            except IndexError:
                url = 'none'
            content = tweet.text
            favorites = tweet.favorite_count
            retweets = tweet.retweet_count
            cur.execute('''
                INSERT OR IGNORE INTO tweets
                (id, leader_id, post_date, url, content, favorite_count,
                retweet_count, html)
                VALUES (?,?,?,?,?,?,?,?)''', (tweetid, ford_id, post_date,
                                              url, content, favorites,
                                              retweets, embed_html,))
        conn.commit()
        conn.close()

    def horwath_tweets(self):
        """Tell the scraper to fetch AndreaHorwath tweets."""
        self.api = twitter.Api(sleep_on_rate_limit=True,
                               consumer_key=self.ckey,
                               consumer_secret=self.cscr,
                               access_token_key=self.atoken,
                               access_token_secret=self.atokscr,)

        # BEGIN Tweet Scrape

        conn = sqlite3.connect('ontario18.db')
        cur = conn.cursor()

        horwath = 'AndreaHorwath'
        horwath_id = 3

        print('Collecting & Adding Tweets to the Databse...', '\n')
        time.sleep(2)
        print('...', '\n')

        tweets = self.api.GetUserTimeline(screen_name=horwath,
                                          include_rts=False, count=150)

        for tweet in tweets:
            self.count = self.count + 1
            print(self.count)
            print('Extracting Tweet Data & Obtaining Embed HTML...')
            tweetid = tweet.id
            """embed_html = html blockquote parsed from status_id"""
            embed_raw = self.api.GetStatusOembed(status_id=tweetid)
            embed_html = embed_raw['html']
            post_date = tweet.created_at
            # catch index errors where no URL supplied from tweet meta
            try:
                url = tweet.urls[0].url
            except IndexError:
                url = 'none'
            content = tweet.text
            favorites = tweet.favorite_count
            retweets = tweet.retweet_count
            cur.execute('''
                INSERT OR IGNORE INTO tweets
                (id, leader_id, post_date, url, content, favorite_count,
                retweet_count, html)
                VALUES (?,?,?,?,?,?,?,?)''', (tweetid, horwath_id, post_date,
                                              url, content, favorites,
                                              retweets, embed_html,))
        conn.commit()
        conn.close()

    def green_tweets(self):
        """Tell the scraper to fetch ‪MikeSchreiner‬ tweets."""
        self.api = twitter.Api(sleep_on_rate_limit=True,
                               consumer_key=self.ckey,
                               consumer_secret=self.cscr,
                               access_token_key=self.atoken,
                               access_token_secret=self.atokscr,)

        # BEGIN Tweet Scrape

        conn = sqlite3.connect('ontario18.db')
        cur = conn.cursor()

        green = '‪MikeSchreiner‬'
        green_id = 4

        print('Collecting & Adding Tweets to the Databse...', '\n')
        time.sleep(2)
        print('...', '\n')

        tweets = self.api.GetUserTimeline(screen_name=green, include_rts=False,
                                          count=150)

        for tweet in tweets:
            self.count = self.count + 1
            print(self.count)
            print('Extracting Tweet Data & Obtaining Embed HTML...')
            tweetid = tweet.id
            """embed_html = html blockquote parsed from status_id"""
            embed_raw = self.api.GetStatusOembed(status_id=tweetid)
            embed_html = embed_raw['html']
            post_date = tweet.created_at
            # catch index errors where no URL supplied from tweet meta
            try:
                url = tweet.urls[0].url
            except IndexError:
                url = 'none'
            content = tweet.text
            favorites = tweet.favorite_count
            retweets = tweet.retweet_count
            cur.execute('''
                INSERT OR IGNORE INTO tweets
                (id, leader_id, post_date, url, content, favorite_count,
                retweet_count, html)
                VALUES (?,?,?,?,?,?,?,?)''', (tweetid, green_id, post_date,
                                              url, content, favorites,
                                              retweets, embed_html,))
        conn.commit()
        conn.close()
