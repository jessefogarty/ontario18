#!/usr/bin/env python3
"""The command line interface for the twitter scraper."""

import click
from bin.scraper import scraper


@click.command()
@click.argument('party', required=1)
def selection(party):
    """party: liberal | conservative | ndp | green."""
    party = party.lower()
    scrape = scraper()

    if party == 'liberal':
        scrape.wynne_tweets()
    elif party == 'conservative':
        scrape.ford_tweets()
    elif party == 'ndp':
        scrape.horwath_tweets
    elif party == 'green':
        scrape.green_tweets


if __name__ == '__main__':
    selection()
